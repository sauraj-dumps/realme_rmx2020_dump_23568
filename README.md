## potato_RMX2020-userdebug 12 SP1A.210812.016 e0e91badc3 test-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2020
- Brand: realme
- Flavor: potato_RMX2020-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: e0e91badc3
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/potato_RMX2020/RMX2020:12/SP1A.210812.016/e0e91badc3:userdebug/test-keys
- OTA version: 
- Branch: potato_RMX2020-userdebug-12-SP1A.210812.016-e0e91badc3-test-keys
- Repo: realme_rmx2020_dump_23568


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
